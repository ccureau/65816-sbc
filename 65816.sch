EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title "65816 Computer"
Date "2020-01-16"
Rev "1.0"
Comp "Chris Cureau"
Comment1 "CPU with Data Bank, Bus Address logic"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 65xx:W65C816SxPL U?
U 1 1 5E1DB487
P 1900 2400
F 0 "U?" H 1900 4131 50  0000 C CNN
F 1 "W65C816SxPL" H 1900 4040 50  0000 C CIB
F 2 "" H 1900 4400 50  0001 C CNN
F 3 "http://www.westerndesigncenter.com/wdc/documentation/w65c816s.pdf" H 1900 4300 50  0001 C CNN
	1    1900 2400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS573 U?
U 1 1 5E1DC04F
P 4500 1750
F 0 "U?" H 4500 2731 50  0000 C CNN
F 1 "74LS573" H 4500 2640 50  0000 C CNN
F 2 "" H 4500 1750 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 4500 1750 50  0001 C CNN
	1    4500 1750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U?
U 1 1 5E1DC5B4
P 4500 3800
F 0 "U?" H 4500 4781 50  0000 C CNN
F 1 "74LS245" H 4500 4690 50  0000 C CNN
F 2 "" H 4500 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 4500 3800 50  0001 C CNN
	1    4500 3800
	1    0    0    -1  
$EndComp
Text GLabel 3700 6400 0    50   Input ~ 0
SYS_CLK
Text GLabel 3700 2150 0    50   Input ~ 0
INVCLK
Text GLabel 950  1500 0    50   Input ~ 0
CLK
Wire Wire Line
	950  1500 1300 1500
$Comp
L 74xx:74LS04 U?
U 1 1 5E1EB0F9
P 4200 6400
F 0 "U?" H 4200 6717 50  0000 C CNN
F 1 "74LS04" H 4200 6626 50  0000 C CNN
F 2 "" H 4200 6400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 4200 6400 50  0001 C CNN
	1    4200 6400
	1    0    0    -1  
$EndComp
Text GLabel 4700 6400 2    50   Output ~ 0
INV_SYS_CLK
Wire Wire Line
	3700 2150 4000 2150
Wire Wire Line
	4000 1250 3850 1250
Wire Wire Line
	4000 1350 3850 1350
Entry Wire Line
	3850 1250 3750 1150
Entry Wire Line
	3850 1350 3750 1250
Entry Wire Line
	3850 1450 3750 1350
Entry Wire Line
	3850 1550 3750 1450
Entry Wire Line
	3850 1650 3750 1550
Entry Wire Line
	3850 1750 3750 1650
Entry Wire Line
	3850 1850 3750 1750
Entry Wire Line
	3850 1950 3750 1850
Wire Wire Line
	3850 1950 4000 1950
Wire Wire Line
	3850 1850 4000 1850
Wire Wire Line
	3850 1750 4000 1750
Wire Wire Line
	3850 1650 4000 1650
Wire Wire Line
	3850 1550 4000 1550
Wire Wire Line
	3850 1450 4000 1450
Wire Bus Line
	3750 1000 3700 1000
Text GLabel 3700 1000 0    50   BiDi ~ 0
D[0..7]
Text Label 3900 1250 0    50   ~ 0
D0
Text Label 3900 1350 0    50   ~ 0
D1
Text Label 3900 1450 0    50   ~ 0
D2
Text Label 3900 1550 0    50   ~ 0
D3
Text Label 3900 1650 0    50   ~ 0
D4
Text Label 3900 1750 0    50   ~ 0
D5
Text Label 3900 1850 0    50   ~ 0
D6
Text Label 3900 1950 0    50   ~ 0
D7
Wire Wire Line
	5000 1250 5150 1250
Wire Wire Line
	5000 1350 5150 1350
Wire Wire Line
	5000 1450 5150 1450
Wire Wire Line
	5000 1550 5150 1550
Wire Wire Line
	5000 1650 5150 1650
Wire Wire Line
	5000 1750 5150 1750
Wire Wire Line
	5000 1850 5150 1850
Wire Wire Line
	5000 1950 5150 1950
Entry Wire Line
	5150 1250 5250 1150
Entry Wire Line
	5150 1350 5250 1250
Entry Wire Line
	5150 1450 5250 1350
Entry Wire Line
	5150 1550 5250 1450
Entry Wire Line
	5150 1650 5250 1550
Entry Wire Line
	5150 1750 5250 1650
Entry Wire Line
	5150 1850 5250 1750
Entry Wire Line
	5150 1950 5250 1850
Wire Bus Line
	5250 1000 5350 1000
Text Label 5000 1250 0    50   ~ 0
BA0
Text Label 5000 1350 0    50   ~ 0
BA1
Text Label 5000 1450 0    50   ~ 0
BA2
Text Label 5000 1550 0    50   ~ 0
BA3
Text Label 5000 1650 0    50   ~ 0
BA4
Text Label 5000 1750 0    50   ~ 0
BA5
Text Label 5000 1850 0    50   ~ 0
BA6
Text Label 5000 1950 0    50   ~ 0
BA7
Text GLabel 5350 1000 2    50   Output ~ 0
BA[0..7]
Wire Wire Line
	4000 3300 3850 3300
Wire Wire Line
	5000 3300 5150 3300
Wire Wire Line
	5000 3400 5150 3400
Wire Wire Line
	5000 3500 5150 3500
Wire Wire Line
	5000 3600 5150 3600
Wire Wire Line
	5000 3700 5150 3700
Wire Wire Line
	5000 3800 5150 3800
Wire Wire Line
	5000 3900 5150 3900
Wire Wire Line
	5000 4000 5150 4000
Text GLabel 3750 4300 0    50   Input ~ 0
INV_SYS_CLK
Wire Wire Line
	3750 4300 4000 4300
Entry Wire Line
	3850 3300 3750 3200
Entry Wire Line
	3850 3400 3750 3300
Entry Wire Line
	3850 3500 3750 3400
Entry Wire Line
	3850 3600 3750 3500
Entry Wire Line
	3850 3700 3750 3600
Entry Wire Line
	3850 3800 3750 3700
Entry Wire Line
	3850 3900 3750 3800
Entry Wire Line
	3850 4000 3750 3900
Entry Wire Line
	5150 3300 5250 3200
Entry Wire Line
	5150 3400 5250 3300
Entry Wire Line
	5150 3500 5250 3400
Entry Wire Line
	5150 3600 5250 3500
Entry Wire Line
	5150 3700 5250 3600
Entry Wire Line
	5150 3800 5250 3700
Entry Wire Line
	5150 3900 5250 3800
Entry Wire Line
	5150 4000 5250 3900
Wire Wire Line
	3850 4000 4000 4000
Wire Wire Line
	3850 3900 4000 3900
Wire Wire Line
	3850 3800 4000 3800
Wire Wire Line
	3850 3700 4000 3700
Wire Wire Line
	3850 3600 4000 3600
Wire Wire Line
	3850 3400 4000 3400
Wire Wire Line
	3850 3500 4000 3500
Wire Bus Line
	5250 3050 5350 3050
Text GLabel 5350 3050 2    50   BiDi ~ 0
DB[0..7]
Text Label 5150 3300 2    50   ~ 0
DB0
Text Label 5150 3400 2    50   ~ 0
DB1
Text Label 5150 3500 2    50   ~ 0
DB2
Text Label 5150 3600 2    50   ~ 0
DB3
Text Label 5150 3700 2    50   ~ 0
DB4
Text Label 5150 3800 2    50   ~ 0
DB5
Text Label 5150 3900 2    50   ~ 0
DB6
Text Label 5150 4000 2    50   ~ 0
DB7
Wire Bus Line
	3750 3050 3650 3050
Text GLabel 3650 3050 0    50   BiDi ~ 0
D[0..7]
Text Label 4000 3300 2    50   ~ 0
D0
Text Label 4000 3400 2    50   ~ 0
D1
Text Label 4000 3500 2    50   ~ 0
D2
Text Label 4000 3600 2    50   ~ 0
D3
Text Label 4000 3700 2    50   ~ 0
D4
Text Label 4000 3800 2    50   ~ 0
D5
Text Label 4000 3900 2    50   ~ 0
D6
Text Label 4000 4000 2    50   ~ 0
D7
Text GLabel 950  2400 0    50   Output ~ 0
RWB
Wire Wire Line
	950  2400 1300 2400
Text GLabel 3750 4200 0    50   Input ~ 0
RWB
Text GLabel 1350 850  0    50   Input ~ 0
+5V
Wire Wire Line
	1350 850  1800 850 
Wire Wire Line
	1800 850  1900 850 
Connection ~ 1800 850 
Text GLabel 4200 950  0    50   Input ~ 0
+5V
Text GLabel 4150 3000 0    50   Input ~ 0
+5V
Wire Wire Line
	4200 950  4500 950 
Wire Wire Line
	4150 3000 4500 3000
Wire Wire Line
	2500 1200 2650 1200
Wire Wire Line
	2500 1300 2650 1300
Wire Wire Line
	2500 1400 2650 1400
Wire Wire Line
	2500 1500 2650 1500
Wire Wire Line
	2500 1600 2650 1600
Wire Wire Line
	2500 1700 2650 1700
Wire Wire Line
	2500 1800 2650 1800
Wire Wire Line
	2500 1900 2650 1900
Wire Wire Line
	2500 2000 2650 2000
Wire Wire Line
	2500 2100 2650 2100
Wire Wire Line
	2500 2200 2650 2200
Wire Wire Line
	2500 2300 2650 2300
Wire Wire Line
	2500 2400 2650 2400
Wire Wire Line
	2500 2500 2650 2500
Wire Wire Line
	2500 2600 2650 2600
Wire Wire Line
	2500 2700 2650 2700
Wire Wire Line
	2500 2900 2650 2900
Wire Wire Line
	2500 3000 2650 3000
Wire Wire Line
	2500 3100 2650 3100
Wire Wire Line
	2500 3200 2650 3200
Wire Wire Line
	2500 3300 2650 3300
Wire Wire Line
	2500 3400 2650 3400
Wire Wire Line
	2500 3500 2650 3500
Wire Wire Line
	2500 3600 2650 3600
Entry Wire Line
	2650 1200 2750 1100
Entry Wire Line
	2650 1300 2750 1200
Entry Wire Line
	2650 1400 2750 1300
Entry Wire Line
	2650 1500 2750 1400
Entry Wire Line
	2650 1600 2750 1500
Entry Wire Line
	2650 1700 2750 1600
Entry Wire Line
	2650 1800 2750 1700
Entry Wire Line
	2650 1900 2750 1800
Entry Wire Line
	2650 2000 2750 1900
Entry Wire Line
	2650 2100 2750 2000
Entry Wire Line
	2650 2200 2750 2100
Entry Wire Line
	2650 2300 2750 2200
Entry Wire Line
	2650 2400 2750 2300
Entry Wire Line
	2650 2500 2750 2400
Entry Wire Line
	2650 2600 2750 2500
Entry Wire Line
	2650 2700 2750 2600
Entry Wire Line
	2650 2900 2750 2800
Entry Wire Line
	2650 3000 2750 2900
Entry Wire Line
	2650 3100 2750 3000
Entry Wire Line
	2650 3200 2750 3100
Entry Wire Line
	2650 3300 2750 3200
Entry Wire Line
	2650 3400 2750 3300
Entry Wire Line
	2650 3500 2750 3400
Entry Wire Line
	2650 3600 2750 3500
Wire Bus Line
	2750 2750 2850 2750
Wire Bus Line
	2750 1000 2850 1000
Text GLabel 2850 2750 2    50   BiDi ~ 0
D[0..7]
Text GLabel 2850 1000 2    50   BiDi ~ 0
A[0..15]
$Comp
L power:GND #PWR?
U 1 1 5E276B77
P 1400 7600
F 0 "#PWR?" H 1400 7350 50  0001 C CNN
F 1 "GND" H 1405 7427 50  0000 C CNN
F 2 "" H 1400 7600 50  0001 C CNN
F 3 "" H 1400 7600 50  0001 C CNN
	1    1400 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 7500 1400 7600
Text GLabel 900  6800 0    50   Input ~ 0
+5V
Wire Wire Line
	1400 6800 1400 6900
$Sheet
S 7450 2200 900  550 
U 5E2BAB8B
F0 "Decoder Logic" 50
F1 "decoder.sch" 50
$EndSheet
$Comp
L 74xx:74LS74 U?
U 1 1 5E2C7771
P 3250 7300
F 0 "U?" H 3250 7781 50  0000 C CNN
F 1 "74LS74" H 3250 7690 50  0000 C CNN
F 2 "" H 3250 7300 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 3250 7300 50  0001 C CNN
	1    3250 7300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U?
U 2 1 5E2C79F4
P 5150 7300
F 0 "U?" H 5150 7781 50  0000 C CNN
F 1 "74LS74" H 5150 7690 50  0000 C CNN
F 2 "" H 5150 7300 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 5150 7300 50  0001 C CNN
	2    5150 7300
	1    0    0    -1  
$EndComp
$Comp
L Oscillator:SG-531 X?
U 1 1 5E2C82E5
P 1400 7200
F 0 "X?" H 1650 7450 50  0000 L CNN
F 1 "SG-531 14.74736MHz" H 1000 7700 50  0000 L CNN
F 2 "Oscillator:Oscillator_SeikoEpson_SG-8002DC" H 1850 6850 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?mode=dl&lang=en&Parts=SG-51P" H 1300 7200 50  0001 C CNN
	1    1400 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  6800 1000 6800
Wire Wire Line
	1100 7200 1000 7200
Wire Wire Line
	1000 7200 1000 6800
Connection ~ 1000 6800
Wire Wire Line
	1000 6800 1400 6800
$Comp
L power:GND #PWR?
U 1 1 5E2D9B33
P 4150 7650
F 0 "#PWR?" H 4150 7400 50  0001 C CNN
F 1 "GND" H 4155 7477 50  0000 C CNN
F 2 "" H 4150 7650 50  0001 C CNN
F 3 "" H 4150 7650 50  0001 C CNN
	1    4150 7650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E2D9DF6
P 4150 7000
F 0 "#PWR?" H 4150 6750 50  0001 C CNN
F 1 "GND" H 4155 6827 50  0000 C CNN
F 2 "" H 4150 7000 50  0001 C CNN
F 3 "" H 4150 7000 50  0001 C CNN
	1    4150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 7600 3250 7650
Text GLabel 3650 7200 2    50   Output ~ 0
SYS_CLK
Wire Wire Line
	3550 7200 3650 7200
Text GLabel 5600 7200 2    50   Output ~ 0
SER_CLK
Wire Wire Line
	5450 7200 5600 7200
Text GLabel 1850 7200 2    50   Output ~ 0
OSC_CLK
Wire Wire Line
	1700 7200 1850 7200
Text GLabel 2800 7200 0    50   Input ~ 0
DQ_BUS_A
Text GLabel 3650 7400 2    50   Output ~ 0
DQ_BUS_A
Wire Wire Line
	3650 7400 3550 7400
Wire Wire Line
	2800 7200 2950 7200
Text GLabel 2800 7300 0    50   Input ~ 0
OSC_CLK
Wire Wire Line
	2800 7300 2950 7300
Wire Wire Line
	3250 7000 4150 7000
Text GLabel 4750 7200 0    50   Input ~ 0
DQ_BUS_B
Text GLabel 5600 7400 2    50   Output ~ 0
DQ_BUS_B
Wire Wire Line
	5600 7400 5450 7400
Wire Wire Line
	4750 7200 4850 7200
Wire Wire Line
	3250 7650 4150 7650
Text GLabel 4750 7300 0    50   Input ~ 0
SYS_CLK
Wire Wire Line
	4750 7300 4850 7300
Wire Wire Line
	4150 7650 5150 7650
Wire Wire Line
	5150 7650 5150 7600
Connection ~ 4150 7650
Wire Wire Line
	4150 7000 5150 7000
Connection ~ 4150 7000
Wire Wire Line
	3700 6400 3900 6400
Wire Wire Line
	4500 6400 4700 6400
$Comp
L 74xx:74LS00 U?
U 1 1 5E354C1A
P 2250 5400
F 0 "U?" H 2250 5725 50  0000 C CNN
F 1 "74LS00" H 2250 5634 50  0000 C CNN
F 2 "" H 2250 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2250 5400 50  0001 C CNN
	1    2250 5400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U?
U 2 1 5E354FB1
P 2250 6050
F 0 "U?" H 2250 6375 50  0000 C CNN
F 1 "74LS00" H 2250 6284 50  0000 C CNN
F 2 "" H 2250 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2250 6050 50  0001 C CNN
	2    2250 6050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U?
U 3 1 5E35570E
P 1450 5300
F 0 "U?" H 1450 5625 50  0000 C CNN
F 1 "74LS00" H 1450 5534 50  0000 C CNN
F 2 "" H 1450 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1450 5300 50  0001 C CNN
	3    1450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 5300 1950 5300
Wire Wire Line
	1950 5500 1850 5500
Wire Wire Line
	1850 5500 1850 5750
Wire Wire Line
	1850 5950 1950 5950
Text GLabel 950  5300 0    50   Input ~ 0
RWB
Text GLabel 1800 6150 0    50   Input ~ 0
RWB
Wire Wire Line
	1800 6150 1950 6150
Wire Wire Line
	1050 5400 1150 5400
Text GLabel 1750 5750 0    50   Input ~ 0
SYS_CLK
Wire Wire Line
	1750 5750 1850 5750
Connection ~ 1850 5750
Wire Wire Line
	1850 5750 1850 5950
Text GLabel 2750 5400 2    50   Output ~ 0
~WE
Text GLabel 2750 6050 2    50   Output ~ 0
~OE
Wire Wire Line
	2550 5400 2750 5400
Wire Wire Line
	2550 6050 2750 6050
Wire Wire Line
	1050 5400 1050 5300
Wire Wire Line
	1050 5200 1150 5200
Wire Wire Line
	950  5300 1050 5300
Connection ~ 1050 5300
Wire Wire Line
	1050 5300 1050 5200
Text GLabel 950  1200 0    50   Output ~ 0
~RESET
Wire Wire Line
	950  1200 1300 1200
Text GLabel 10050 1200 2    50   Output ~ 0
+5V
Text GLabel 10050 1350 2    50   Output ~ 0
GND
Wire Wire Line
	9900 1200 10050 1200
Wire Wire Line
	10050 1350 9900 1350
Text GLabel 3700 2250 0    50   Input ~ 0
GND
Wire Wire Line
	3700 2250 3950 2250
Wire Wire Line
	4500 2550 3950 2550
Wire Wire Line
	3950 2550 3950 2250
Connection ~ 3950 2250
Wire Wire Line
	3950 2250 4000 2250
Text GLabel 1550 4100 0    50   Input ~ 0
GND
Text GLabel 4200 4600 0    50   Input ~ 0
GND
Wire Wire Line
	4200 4600 4500 4600
Wire Wire Line
	1550 4100 1800 4100
Wire Wire Line
	2000 4100 2000 3950
Wire Wire Line
	1900 3950 1900 4100
Connection ~ 1900 4100
Wire Wire Line
	1900 4100 2000 4100
Wire Wire Line
	1800 3950 1800 4100
Connection ~ 1800 4100
Wire Wire Line
	1800 4100 1900 4100
Wire Wire Line
	3750 4200 4000 4200
$Sheet
S 9050 1050 850  500 
U 5E2EFE2E
F0 "Power and Reset" 50
F1 "power.sch" 50
F2 "+5V" O R 9900 1200 50 
F3 "GND" O R 9900 1350 50 
F4 "RESET" O L 9050 1200 50 
F5 "~RESET" O L 9050 1350 50 
$EndSheet
Text GLabel 8950 1200 0    50   Output ~ 0
RESET
Text GLabel 8950 1350 0    50   Output ~ 0
~RESET
Wire Wire Line
	8950 1200 9050 1200
Wire Wire Line
	8950 1350 9050 1350
Text GLabel 8100 1050 2    50   Input ~ 0
+5V
Text GLabel 8100 1200 2    50   Input ~ 0
~OE
Text GLabel 8100 1350 2    50   Input ~ 0
RESET
Text GLabel 8100 1500 2    50   Input ~ 0
GND
Text GLabel 7000 1400 0    50   Input ~ 0
~WE
Text GLabel 7000 1300 0    50   Input ~ 0
~DUARTEN
Text GLabel 7000 1100 0    50   BiDi ~ 0
DB[0..7]
Text GLabel 7000 1200 0    50   Input ~ 0
A[0..15]
Wire Wire Line
	8000 1500 8100 1500
Wire Wire Line
	8000 1050 8100 1050
Wire Wire Line
	8000 1350 8100 1350
Wire Wire Line
	8000 1200 8100 1200
Wire Wire Line
	7100 1400 7000 1400
Wire Wire Line
	7100 1300 7000 1300
Wire Wire Line
	7100 1200 7000 1200
Wire Wire Line
	7100 1100 7000 1100
$Sheet
S 7100 1000 900  650 
U 5E284345
F0 "DUART " 50
F1 "duart.sch" 50
F2 "DB[0..7]" B L 7100 1100 50 
F3 "A[0..15]" B L 7100 1200 50 
F4 "~DUARTEN" I L 7100 1300 50 
F5 "~WE" I L 7100 1400 50 
F6 "~OE" I R 8000 1200 50 
F7 "RESET" I R 8000 1350 50 
F8 "~IRQ" O L 7100 1500 50 
F9 "+5V" I R 8000 1050 50 
F10 "GND" I R 8000 1500 50 
$EndSheet
Wire Wire Line
	7100 1500 7000 1500
Text GLabel 7000 1500 0    50   Output ~ 0
~IRQ
Wire Bus Line
	2750 2750 2750 3500
Wire Bus Line
	5250 3050 5250 3900
Wire Bus Line
	3750 3050 3750 3900
Wire Bus Line
	5250 1000 5250 1850
Wire Bus Line
	3750 1000 3750 1850
Wire Bus Line
	2750 1000 2750 2600
$EndSCHEMATC
