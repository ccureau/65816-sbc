EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:L7805 U?
U 1 1 5E2F012C
P 2400 1150
F 0 "U?" H 2400 1392 50  0000 C CNN
F 1 "L7805" H 2400 1301 50  0000 C CNN
F 2 "" H 2425 1000 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2400 1100 50  0001 C CNN
	1    2400 1150
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D?
U 1 1 5E2F039D
P 1700 1150
F 0 "D?" H 1700 934 50  0000 C CNN
F 1 "1N4007" H 1700 1025 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1700 975 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 1700 1150 50  0001 C CNN
	1    1700 1150
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C?
U 1 1 5E2F09E3
P 1900 1500
F 0 "C?" H 2018 1546 50  0000 L CNN
F 1 "220 uF" H 2018 1455 50  0000 L CNN
F 2 "" H 1938 1350 50  0001 C CNN
F 3 "~" H 1900 1500 50  0001 C CNN
	1    1900 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5E2F0AFC
P 2950 1500
F 0 "C?" H 3068 1546 50  0000 L CNN
F 1 "0.1 uF" H 3068 1455 50  0000 L CNN
F 2 "" H 2988 1350 50  0001 C CNN
F 3 "~" H 2950 1500 50  0001 C CNN
	1    2950 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E2F11AD
P 1200 1000
F 0 "#PWR?" H 1200 850 50  0001 C CNN
F 1 "+12V" H 1215 1173 50  0000 C CNN
F 2 "" H 1200 1000 50  0001 C CNN
F 3 "" H 1200 1000 50  0001 C CNN
	1    1200 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E2F13B0
P 1200 1900
F 0 "#PWR?" H 1200 1650 50  0001 C CNN
F 1 "GND" H 1205 1727 50  0000 C CNN
F 2 "" H 1200 1900 50  0001 C CNN
F 3 "" H 1200 1900 50  0001 C CNN
	1    1200 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5E2F281B
P 3450 1300
F 0 "D?" V 3489 1183 50  0000 R CNN
F 1 "LED" V 3398 1183 50  0000 R CNN
F 2 "" H 3450 1300 50  0001 C CNN
F 3 "~" H 3450 1300 50  0001 C CNN
	1    3450 1300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E2F2CD5
P 3450 1650
F 0 "R?" H 3509 1696 50  0000 L CNN
F 1 "220 Ohm" H 3509 1605 50  0000 L CNN
F 2 "" H 3450 1650 50  0001 C CNN
F 3 "~" H 3450 1650 50  0001 C CNN
	1    3450 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1000 1200 1150
Wire Wire Line
	1200 1150 1550 1150
Wire Wire Line
	3450 1450 3450 1550
Wire Wire Line
	1200 1900 1200 1800
Wire Wire Line
	3450 1800 3450 1750
Wire Wire Line
	2700 1150 2950 1150
Wire Wire Line
	1850 1150 1900 1150
Wire Wire Line
	1200 1800 1900 1800
Wire Wire Line
	1900 1350 1900 1150
Connection ~ 1900 1150
Wire Wire Line
	1900 1150 2100 1150
Wire Wire Line
	1900 1650 1900 1800
Connection ~ 1900 1800
Wire Wire Line
	1900 1800 2950 1800
Wire Wire Line
	2950 1350 2950 1150
Connection ~ 2950 1150
Wire Wire Line
	2950 1150 3450 1150
Wire Wire Line
	2950 1650 2950 1800
Connection ~ 2950 1800
Wire Wire Line
	2950 1800 3450 1800
Text GLabel 4100 1150 2    50   Output ~ 0
+5V
Text GLabel 4100 1800 2    50   Output ~ 0
GND
Wire Wire Line
	4100 1800 3450 1800
Connection ~ 3450 1800
Wire Wire Line
	3450 1150 4100 1150
Connection ~ 3450 1150
Text HLabel 5700 2950 2    50   Output ~ 0
+5V
Text HLabel 5700 3450 2    50   Output ~ 0
GND
Text GLabel 1150 2950 0    50   Input ~ 0
+5V
Text GLabel 1150 3450 0    50   Input ~ 0
GND
Wire Wire Line
	1150 2950 1350 2950
Wire Wire Line
	5700 3450 1800 3450
$Comp
L Device:C_Small C?
U 1 1 5E2FDDBC
P 1350 3200
F 0 "C?" H 1442 3246 50  0000 L CNN
F 1 "0.1 uF" H 1442 3155 50  0000 L CNN
F 2 "" H 1350 3200 50  0001 C CNN
F 3 "~" H 1350 3200 50  0001 C CNN
	1    1350 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E2FDF89
P 1800 3200
F 0 "C?" H 1892 3246 50  0000 L CNN
F 1 "0.1 uF" H 1892 3155 50  0000 L CNN
F 2 "" H 1800 3200 50  0001 C CNN
F 3 "~" H 1800 3200 50  0001 C CNN
	1    1800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3100 1350 2950
Connection ~ 1350 2950
Wire Wire Line
	1350 2950 1800 2950
Wire Wire Line
	1800 3100 1800 2950
Connection ~ 1800 2950
Wire Wire Line
	1800 2950 5700 2950
Wire Wire Line
	1350 3300 1350 3450
Connection ~ 1350 3450
Wire Wire Line
	1350 3450 1150 3450
Wire Wire Line
	1800 3300 1800 3450
Connection ~ 1800 3450
Wire Wire Line
	1800 3450 1350 3450
$Comp
L 74xx:74LS04 U?
U 2 1 5E357381
P 9600 1500
AR Path="/5E357381" Ref="U?"  Part="2" 
AR Path="/5E2EFE2E/5E357381" Ref="U?"  Part="2" 
F 0 "U?" H 9600 1817 50  0000 C CNN
F 1 "74LS04" H 9600 1726 50  0000 C CNN
F 2 "" H 9600 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9600 1500 50  0001 C CNN
	2    9600 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 1500 10050 1500
Text GLabel 8350 1400 2    50   Input ~ 0
+5V
Text GLabel 5750 1400 0    50   Input ~ 0
GND
Text HLabel 10050 1500 2    50   Output ~ 0
RESET
Text HLabel 10050 1050 2    50   Output ~ 0
~RESET
Wire Wire Line
	10050 1050 9200 1050
Wire Wire Line
	9200 1050 9200 1500
Wire Wire Line
	9200 1500 9300 1500
$Comp
L maxim-ds1233:DS1233-5+ U?
U 1 1 5E35F3BD
P 5850 1400
F 0 "U?" H 7050 1787 60  0000 C CNN
F 1 "DS1233-5+" H 7050 1681 60  0000 C CNN
F 2 "21-0248A" H 7050 1640 60  0001 C CNN
F 3 "" H 5850 1400 60  0000 C CNN
	1    5850 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1400 8250 1400
Wire Wire Line
	5750 1400 5850 1400
$Comp
L Device:C_Small C?
U 1 1 5E362040
P 8350 1750
F 0 "C?" H 8442 1796 50  0000 L CNN
F 1 "0.01 uF" H 8442 1705 50  0000 L CNN
F 2 "" H 8350 1750 50  0001 C CNN
F 3 "~" H 8350 1750 50  0001 C CNN
	1    8350 1750
	1    0    0    -1  
$EndComp
Text GLabel 8650 2150 0    50   Input ~ 0
GND
Wire Wire Line
	8250 1500 8350 1500
Wire Wire Line
	8800 1900 8800 2000
Wire Wire Line
	8800 2150 8650 2150
Wire Wire Line
	8350 1850 8350 2000
Wire Wire Line
	8350 2000 8800 2000
Connection ~ 8800 2000
Wire Wire Line
	8800 2000 8800 2150
Wire Wire Line
	8350 1650 8350 1500
Connection ~ 8350 1500
Connection ~ 9200 1500
Wire Wire Line
	8800 1500 9200 1500
Wire Wire Line
	8350 1500 8800 1500
Connection ~ 8800 1500
$Comp
L Switch:SW_Push SW?
U 1 1 5E3592D4
P 8800 1700
F 0 "SW?" V 8754 1848 50  0000 L CNN
F 1 "SW_Push" V 8845 1848 50  0000 L CNN
F 2 "" H 8800 1900 50  0001 C CNN
F 3 "~" H 8800 1900 50  0001 C CNN
	1    8800 1700
	0    1    1    0   
$EndComp
$EndSCHEMATC
