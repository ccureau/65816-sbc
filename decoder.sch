EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS00 U?
U 3 1 5E2BB66B
P 5650 3450
F 0 "U?" H 5650 3775 50  0000 C CNN
F 1 "74LS00" H 5650 3684 50  0000 C CNN
F 2 "" H 5650 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5650 3450 50  0001 C CNN
	3    5650 3450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U?
U 1 1 5E2BB9D5
P 4750 2750
F 0 "U?" H 4750 3075 50  0000 C CNN
F 1 "74LS00" H 4750 2984 50  0000 C CNN
F 2 "" H 4750 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 4750 2750 50  0001 C CNN
	1    4750 2750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U?
U 2 1 5E2BBC9D
P 5650 2850
F 0 "U?" H 5650 3175 50  0000 C CNN
F 1 "74LS00" H 5650 3084 50  0000 C CNN
F 2 "" H 5650 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5650 2850 50  0001 C CNN
	2    5650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2750 5350 2750
Text HLabel 4050 2650 0    50   Input ~ 0
R~W
Wire Wire Line
	4050 2650 4150 2650
Wire Wire Line
	4450 2850 4450 3550
Wire Wire Line
	4450 3550 5350 3550
Wire Wire Line
	4450 2850 4150 2850
Wire Wire Line
	4150 2850 4150 2650
Connection ~ 4450 2850
Connection ~ 4150 2650
Wire Wire Line
	4150 2650 4450 2650
Text HLabel 5050 3150 0    50   Input ~ 0
CLK
$EndSCHEMATC
