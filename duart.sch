EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 4
Title "DUART"
Date ""
Rev "1.0"
Comp "Chris Cureau"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L nxp-uarts:SCC28L92 U?
U 1 1 5E289C22
P 2050 1750
F 0 "U?" H 3100 1481 50  0000 C CNN
F 1 "SCC28L92" H 3100 1390 50  0000 C CNN
F 2 "" H 2050 1750 50  0001 C CNN
F 3 "" H 2050 1750 50  0001 C CNN
	1    2050 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2450 2400 2450
Wire Wire Line
	2550 3400 2400 3400
Entry Wire Line
	2400 2450 2300 2350
Entry Wire Line
	2400 2550 2300 2450
Entry Wire Line
	2400 2650 2300 2550
Entry Wire Line
	2400 2750 2300 2650
Entry Wire Line
	2400 2850 2300 2750
Entry Wire Line
	2400 2950 2300 2850
Entry Wire Line
	2400 3050 2300 2950
Entry Wire Line
	2400 3150 2300 3050
Entry Wire Line
	2400 3400 2300 3300
Entry Wire Line
	2400 3500 2300 3400
Entry Wire Line
	2400 3600 2300 3500
Entry Wire Line
	2400 3700 2300 3600
Wire Wire Line
	2400 3700 2550 3700
Wire Wire Line
	2400 3500 2550 3500
Wire Wire Line
	2400 3600 2550 3600
Wire Wire Line
	2400 3150 2550 3150
Wire Wire Line
	2400 3050 2550 3050
Wire Wire Line
	2400 2950 2550 2950
Wire Wire Line
	2400 2850 2550 2850
Wire Wire Line
	2400 2550 2550 2550
Wire Wire Line
	2400 2650 2550 2650
Wire Wire Line
	2400 2750 2550 2750
Wire Bus Line
	2300 3200 2250 3200
Wire Bus Line
	2300 2250 2250 2250
Text HLabel 2250 2250 0    50   BiDi ~ 0
DB[0..7]
Text HLabel 2250 3200 0    50   BiDi ~ 0
A[0..15]
NoConn ~ 2550 4900
Text GLabel 2200 4800 0    50   Input ~ 0
SERCLK
Wire Wire Line
	2200 4800 2550 4800
Wire Wire Line
	2150 4650 2550 4650
Wire Wire Line
	2150 4150 2550 4150
Wire Wire Line
	2150 4050 2550 4050
Text HLabel 2150 3950 0    50   Input ~ 0
~DUARTEN
Wire Wire Line
	2150 3950 2550 3950
$Comp
L maxim-parts:MAX248 U?
U 1 1 5E420C91
P 5550 3050
F 0 "U?" H 5450 4131 50  0000 C CNN
F 1 "MAX248" H 5450 4040 50  0000 C CNN
F 2 "" H 5100 4000 50  0001 C CNN
F 3 "" H 5100 4000 50  0001 C CNN
	1    5550 3050
	1    0    0    -1  
$EndComp
Text HLabel 2150 4050 0    50   Input ~ 0
~WE
Text HLabel 2150 4150 0    50   Input ~ 0
~OE
Wire Wire Line
	3650 2450 3750 2450
Wire Wire Line
	3650 2550 3750 2550
Wire Wire Line
	3650 2700 3750 2700
Wire Wire Line
	3650 2800 3750 2800
Text GLabel 3750 2550 2    50   Output ~ 0
TxDB
Text GLabel 3750 2450 2    50   Output ~ 0
TxDA
Text GLabel 3750 2700 2    50   Input ~ 0
RxDA
Text GLabel 3750 2800 2    50   Input ~ 0
RxDB
Text HLabel 2150 4450 0    50   Input ~ 0
RESET
Wire Wire Line
	2150 4450 2550 4450
Text HLabel 2150 4350 0    50   Output ~ 0
~IRQ
Wire Wire Line
	2550 4350 2150 4350
Text HLabel 2150 4650 0    50   Input ~ 0
+5V
Text HLabel 2800 2200 0    50   Input ~ 0
+5V
Text HLabel 5100 2150 0    50   Input ~ 0
+5V
Wire Wire Line
	5100 2150 5450 2150
Wire Wire Line
	2800 2200 3100 2200
Text HLabel 2800 5250 0    50   Input ~ 0
GND
Text HLabel 5200 4950 0    50   Input ~ 0
GND
Wire Wire Line
	5200 4950 5500 4950
Wire Wire Line
	5500 4950 5500 4900
Wire Wire Line
	2800 5250 3100 5250
Wire Wire Line
	3100 5250 3100 5200
NoConn ~ 3650 4700
NoConn ~ 3650 4600
NoConn ~ 3650 4500
NoConn ~ 3650 4400
NoConn ~ 3650 3550
Wire Wire Line
	4900 2350 4750 2350
Wire Wire Line
	4900 2450 4750 2450
Wire Wire Line
	4900 2550 4750 2550
Wire Wire Line
	4900 2650 4750 2650
Wire Wire Line
	4900 2800 4750 2800
Wire Wire Line
	4900 2900 4750 2900
Wire Wire Line
	4900 3000 4750 3000
Wire Wire Line
	4900 3100 4750 3100
Wire Wire Line
	4900 3250 4750 3250
Wire Wire Line
	4900 3350 4750 3350
Wire Wire Line
	4900 3450 4750 3450
Wire Wire Line
	4900 3550 4750 3550
Wire Wire Line
	4900 3700 4750 3700
Wire Wire Line
	4900 3800 4750 3800
Wire Wire Line
	4900 3900 4750 3900
Wire Wire Line
	4900 4000 4750 4000
Wire Wire Line
	4900 4150 4750 4150
Wire Wire Line
	4900 4250 4750 4250
Wire Wire Line
	6000 2350 6150 2350
Wire Wire Line
	6000 2450 6150 2450
Wire Wire Line
	6000 2550 6150 2550
Wire Wire Line
	6000 2650 6150 2650
Wire Wire Line
	6000 2800 6150 2800
Wire Wire Line
	6000 2900 6150 2900
Wire Wire Line
	6000 3000 6150 3000
Wire Wire Line
	6000 3100 6150 3100
Wire Wire Line
	6000 3250 6150 3250
Wire Wire Line
	6000 3350 6150 3350
Wire Wire Line
	6000 3450 6150 3450
Wire Wire Line
	6000 3550 6150 3550
Wire Wire Line
	6000 3700 6150 3700
Wire Wire Line
	6000 3800 6150 3800
Wire Wire Line
	6000 3900 6150 3900
Wire Wire Line
	6000 4000 6150 4000
Wire Wire Line
	6000 4150 6150 4150
Wire Wire Line
	6000 4250 6150 4250
Text GLabel 4750 2350 0    50   Input ~ 0
TxDA
Text GLabel 4750 2450 0    50   Input ~ 0
TxDB
Text GLabel 4750 3250 0    50   Output ~ 0
RxDA
Text GLabel 4750 3350 0    50   Output ~ 0
RxDB
Wire Wire Line
	3650 3150 3750 3150
Wire Wire Line
	3650 3250 3750 3250
Wire Wire Line
	3650 3350 3750 3350
Wire Wire Line
	3650 3450 3750 3450
Wire Wire Line
	3650 3650 3750 3650
Wire Wire Line
	3650 3750 3750 3750
Text GLabel 3750 3150 2    50   Input ~ 0
CTSA
Text GLabel 3750 3250 2    50   Input ~ 0
CTSB
Text GLabel 3750 3350 2    50   Input ~ 0
DCDA
Text GLabel 3750 3450 2    50   Input ~ 0
DCDB
Text GLabel 3750 3650 2    50   Input ~ 0
DSRB
Text GLabel 3750 3750 2    50   Input ~ 0
RxDB
Wire Wire Line
	3650 4000 3750 4000
Wire Wire Line
	3650 4100 3750 4100
Wire Wire Line
	3650 4200 3750 4200
Wire Wire Line
	3650 4300 3750 4300
Text GLabel 3750 4000 2    50   Output ~ 0
TxDB
Text GLabel 3750 4100 2    50   Output ~ 0
TxDB
Text GLabel 3750 4200 2    50   Output ~ 0
TxDB
Text GLabel 3750 4300 2    50   Output ~ 0
TxDB
NoConn ~ 6150 3000
NoConn ~ 6150 3100
Text HLabel 4750 3000 0    50   Input ~ 0
GND
Text HLabel 4750 3100 0    50   Input ~ 0
GND
Text GLabel 4750 2550 0    50   Input ~ 0
RTSA
Text GLabel 4750 2650 0    50   Input ~ 0
RTSB
Text GLabel 4750 2800 0    50   Input ~ 0
DTRA
Text GLabel 4750 2900 0    50   Input ~ 0
DTRB
Text GLabel 4750 3450 0    50   Output ~ 0
CTSA
Text GLabel 4750 3550 0    50   Output ~ 0
CTSB
Text GLabel 4750 3700 0    50   Output ~ 0
DSRA
Text GLabel 4750 3800 0    50   Output ~ 0
DSRB
Text GLabel 4750 3900 0    50   Output ~ 0
DCDB
Text GLabel 4750 4000 0    50   Output ~ 0
RxDB
Text HLabel 4750 4150 0    50   Input ~ 0
GND
Text HLabel 4750 4250 0    50   Input ~ 0
GND
Text HLabel 6150 4150 2    50   Input ~ 0
GND
Text HLabel 6150 4250 2    50   Input ~ 0
GND
$Comp
L Device:CP C?
U 1 1 5E3ED5FE
P 4600 4750
F 0 "C?" H 4718 4796 50  0000 L CNN
F 1 "1 uF" H 4718 4705 50  0000 L CNN
F 2 "" H 4638 4600 50  0001 C CNN
F 3 "~" H 4600 4750 50  0001 C CNN
	1    4600 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5E3ED81D
P 4200 4550
F 0 "C?" H 4318 4596 50  0000 L CNN
F 1 "1 uF" H 4318 4505 50  0000 L CNN
F 2 "" H 4238 4400 50  0001 C CNN
F 3 "~" H 4200 4550 50  0001 C CNN
	1    4200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5E3EE37C
P 6300 4500
F 0 "C?" V 6500 4350 50  0000 C CNN
F 1 "1 uF" V 6400 4300 50  0000 C CNN
F 2 "" H 6338 4350 50  0001 C CNN
F 3 "~" H 6300 4500 50  0001 C CNN
	1    6300 4500
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C?
U 1 1 5E3EEBC6
P 6150 4750
F 0 "C?" H 6268 4796 50  0000 L CNN
F 1 "1 uF" H 6268 4705 50  0000 L CNN
F 2 "" H 6188 4600 50  0001 C CNN
F 3 "~" H 6150 4750 50  0001 C CNN
	1    6150 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4500 6150 4500
Wire Wire Line
	6000 4600 6150 4600
Wire Wire Line
	5500 4950 6150 4950
Wire Wire Line
	6150 4950 6150 4900
Connection ~ 5500 4950
Text HLabel 6450 4500 2    50   Input ~ 0
+5V
Wire Wire Line
	4200 4400 4900 4400
Wire Wire Line
	4900 4500 4500 4500
Wire Wire Line
	4500 4500 4500 4700
Wire Wire Line
	4500 4700 4200 4700
Wire Wire Line
	4600 4600 4900 4600
Wire Wire Line
	4900 4700 4900 4900
Wire Wire Line
	4900 4900 4600 4900
Text GLabel 6150 2350 2    50   Output ~ 0
TxDa
Text GLabel 6150 2450 2    50   Output ~ 0
TxDb
Text GLabel 6150 2550 2    50   Output ~ 0
RTSa
Text GLabel 6150 2650 2    50   Output ~ 0
RTSb
Text GLabel 6150 2800 2    50   Output ~ 0
DTRa
Text GLabel 6150 2900 2    50   Output ~ 0
DTRb
Text GLabel 6150 3250 2    50   Input ~ 0
RxDa
Text GLabel 6150 3350 2    50   Input ~ 0
RxDb
Text GLabel 6150 3450 2    50   Input ~ 0
CTSa
Text GLabel 6150 3550 2    50   Input ~ 0
CTSb
Text GLabel 6150 3700 2    50   Input ~ 0
DSRa
Text GLabel 6150 3800 2    50   Input ~ 0
DSRb
Text GLabel 6150 3900 2    50   Input ~ 0
DCDa
Text GLabel 6150 4000 2    50   Input ~ 0
DCDb
$Comp
L Connector:DB9_Male_MountingHoles J?
U 1 1 5E421ED7
P 8400 2400
F 0 "J?" H 8580 2402 50  0000 L CNN
F 1 "SER_PORT_A" H 8580 2311 50  0000 L CNN
F 2 "" H 8400 2400 50  0001 C CNN
F 3 " ~" H 8400 2400 50  0001 C CNN
	1    8400 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Male_MountingHoles J?
U 1 1 5E4221E3
P 8400 3900
F 0 "J?" H 8580 3902 50  0000 L CNN
F 1 "SER_PORT_B" H 8580 3811 50  0000 L CNN
F 2 "" H 8400 3900 50  0001 C CNN
F 3 " ~" H 8400 3900 50  0001 C CNN
	1    8400 3900
	1    0    0    -1  
$EndComp
Text GLabel 8100 2800 0    50   Output ~ 0
DCDa
Text GLabel 8100 4300 0    50   Output ~ 0
DCDb
Text GLabel 8100 2600 0    50   Output ~ 0
RxDa
Text GLabel 8100 4100 0    50   Output ~ 0
RxDb
Text GLabel 8100 2400 0    50   Input ~ 0
TxDa
Text GLabel 8100 3900 0    50   Input ~ 0
TxDb
Text GLabel 8100 2200 0    50   Input ~ 0
DTRa
Text GLabel 8100 3700 0    50   Input ~ 0
DTRa
Text HLabel 8100 3500 0    50   Input ~ 0
GND
Text HLabel 8100 2000 0    50   Input ~ 0
GND
Text GLabel 8100 2700 0    50   Output ~ 0
DSRa
Text GLabel 8100 4200 0    50   Output ~ 0
DSRb
Text GLabel 8100 2500 0    50   Input ~ 0
RTSa
Text GLabel 8100 4000 0    50   Input ~ 0
RTSa
Text GLabel 8100 2300 0    50   Output ~ 0
CTSa
Text GLabel 8100 3800 0    50   Output ~ 0
CTSa
NoConn ~ 8100 2100
NoConn ~ 8100 3600
Wire Bus Line
	2300 3200 2300 3600
Wire Bus Line
	2300 2250 2300 3050
$EndSCHEMATC
